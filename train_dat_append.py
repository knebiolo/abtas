# import modules
import os
import sqlite3
import pandas as pd
import warnings
warnings.filterwarnings('ignore')


#set script parameters
inputWS = r"J:\1210\005\Calcs\Studies\3_3_19\2018\Data"
outputWS = r"J:\1503\212\Calcs\Scotland_Fall2019\Data"
db_in = 'ultrasound_2018 - Copy.db'                                                  # what is the name of the project database?
db_out = 'Scotland_Eel_2019.db'

# get the training data with SQL 
conn = sqlite3.connect(os.path.join(inputWS,db_in))
c = conn.cursor()
sql = "SELECT * FROM tblTrain "
train_dat = pd.read_sql(sql,con = conn)
c.close()

# rename some columns
train_dat.rename(columns = {'lagB':'lag','lagBdiff':'lagDiff'}, inplace = True)

# append to output database
conn = sqlite3.connect(os.path.join(inputWS,db_out))
c = conn.cursor()
train_dat.to_sql('tblTrain',con = conn, index = False, if_exists = 'append')
c.close()
