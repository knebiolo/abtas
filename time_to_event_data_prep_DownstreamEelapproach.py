# -*- coding: utf-8 -*-
"""
Created on Fri Dec  6 10:30:08 2019

@author: Alex Malvezzi
"""

# import modules
import abtas
import os
import warnings
warnings.filterwarnings('ignore')
# set up script parameters
proj_dir = r'J:\1503\212\Calcs\Scotland_Fall2019'                            
dbName = 'Scotland_Eel_2019 - Copy.db'                                                   
projectDB = os.path.join(proj_dir,'Data',dbName)
# what is the output directory?                         
outputWS = os.path.join(proj_dir,'Output')
o_fileName_cov = "downstreamapproach_cov.csv"
o_fileName = "downstreamapproach.csv"
# what is the Node to State relationship - use Python dictionary 
node_to_state = {'S02':1,'S03':1,'S04':1,'S05':1,'S07':2,'S08':2}
recList = ['T02','T03','T04','T10','T06','T07']
# Step 1, create time to event data class - we only need to feed it the directory and file name of input data
tte = abtas.time_to_event(recList,(node_to_state),projectDB)
print ("Step 1 Complete, Data Class Finished")
# Step 2, format data - with covariates
tte.data_prep(os.path.join(outputWS,o_fileName_cov), time_dependent_covariates = True)
print ("Time to Event data formatted for time dependent covariates")
# Step 3, format data - without covariates
tte.data_prep(os.path.join(outputWS,o_fileName))
print ("Time to Event data formated without time dependent covariates")
# Step 4, generate a summary
tte.summary()
print ("Data formatting complete, proceed to R for Time to Event Modeling")