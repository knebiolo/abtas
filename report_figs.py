import time
import os
import numpy as np
import sys
sys.path.append(r"U:\Software\biotas")
import biotas
import warnings
warnings.filterwarnings('ignore')

t0 = time.time()
# What receiver type are you assessing accuracy for?
recType = 'orion'                                                              # what is the receiver type?
proj_dir = r'C:\Users\Kevin Nebiolo\Desktop\Articles for Submission\Ted'             # what is the project directory?
dbName = 'manuscript.db'                                            # whad did you call the database?
projectDB = os.path.join(proj_dir,'Data',dbName)
scratch_dir = os.path.join(proj_dir,'Output','Scratch')
figure_ws = os.path.join(proj_dir,'Output','Figures')


#train_stats = biotas.training_results(recType,projectDB,figure_ws)#,site)
#train_stats.train_stats() 
class_stats = biotas.classification_results(recType,projectDB,figure_ws)#,reclass_iter = class_iter)
class_stats.classify_stats()  